import pytest
from flask import json, jsonify, request
from project import create_app
from project.constants import *

app = create_app("conf/custom/custom_application.conf", 5000)


@pytest.fixture
def client():
    with app.test_client() as client:
        app.config['TESTING'] = True
        yield client


def test_github_base_url(client):
    response = client.get("/")
    data = json.loads(response.data)
    assert data['code_search_url'] == 'https://api.github.com/search/code?q={query}{&page,per_page,sort,order}'
    assert response.status_code == 200


def test_netflix_base_url(client):
    response = client.get(NETFLIX_BASE_URL)
    data = json.loads(response.data)
    assert data['url'] == 'https://api.github.com/orgs/Netflix'
    assert response.status_code == 200


def test_netflix_members_url(client):
    response = client.get(NETFLIX_MEMBERS_URL)
    data = json.loads(response.data)
    assert len(data) == 19
    assert response.status_code == 200


def test_netflix_repos_url(client):
    response = client.get(NETFLIX_REPOS_URL)
    data = json.loads(response.data)
    assert len(data) == 186
    assert response.status_code == 200


def test_top_5_forks(client):
    response = client.get('/view/top/5/forks')
    data = json.loads(response.data)
    assert len(data) == 5
    assert response.status_code == 200


def test_top_5_stars(client):
    response = client.get('/view/top/5/stars')
    data = json.loads(response.data)
    assert len(data) == 5
    assert response.status_code == 200


def test_top_5_last_updated(client):
    response = client.get('/view/top/5/last_updated')
    data = json.loads(response.data)
    assert len(data) == 5
    assert response.status_code == 200


def test_top_5_open_issues(client):
    response = client.get('/view/top/5/open_issues')
    data = json.loads(response.data)
    assert len(data) == 5
    assert response.status_code == 200


def test_user_data(client):
    response = client.get('/users/chali/orgs')
    data = json.loads(response.data)
    assert data[1]['login'] == "nebula-plugins"
    assert response.status_code == 200
