git api reads cache

# README #

A python web-server that caches data from a few Github APIs and provides custom endpoints serving the data from cache. 
For any other github endpoint this web-server acts as a proxy. By default, the web-server will run on localhost with 
port 5000. Port can be supplied as command-line argument

### What is this repository for? ###

* Quick summary
    * Data from following endpoints are cached
        * /
        * /orgs/Netflix
        * /orgs/Netflix/members
        * /orgs/Netflix/repos

    * Provides the following custom endpoints serving the data from cache.
        * /view/top/N/forks
        * /view/top/N/last_updated
        * /view/top/N/open_issues
        * /view/top/N/stars
       
    * Prometheus metrics are supported via /metrics endpoint

### How do I get set up? ###

* Summary of set up
* Dependencies
  * Memcached service to be running. Configure the host and port in conf/default/application_default.conf
* How to run tests
  * GITHUB_API_TOKEN="\<api token\>" pytest
* Deployment instructions
  * Install virtualenv and requirements
    * pip install virtualenv
    * virtualenv venv
    * source venv/bin/activate
    * pip install -r requirements.txt
    * set the environment variable GITHUB_API_TOKEN
    * To run in docker: docker-compose up --build
    * To run in shell: 
        * Start memcached at localhost:12111
        * python run.py --port=5000 --conf=conf/custom/custom_application.conf

* Design
* Assumptions
  * Data size to be cached is not too big
  * Data growth rate is close to zero
  * Load balancing will be done on the client side - no throttling in the service as of now
  * Cache refresh rate is 1 hour although that can be configured in the conf files.
  * Cache invalidation happens when refresh happens
 
 Service implements a python flask based web-server with the endpoints listed above. Response data from the github
 APIs are serialized and stored in a memcached instance. When the server starts for the first time, it fetches the data
 from github APIs and builds the cache. After the web-server is running, any request for the supported end-points are
 served from the cache. For all other endpoints, it is a pass-through to github. 
 
 Service exposes prometheus style metrics endpoints which can be scraped.
 
 For load-balancing, we could launch multiple instances of docker containers using the --scale option behind a nginx
 server.  