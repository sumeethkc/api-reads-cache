import atexit
from logging.config import dictConfig

from apscheduler.schedulers.background import BackgroundScheduler
from flask import Flask, Response, request, jsonify
from prometheus_flask_exporter import PrometheusMetrics

from project.config.cache_server_config import CacheServerConfig
from project.constants import *
from project.persistence.cache import Cache
from project.service.apiservice import ApiService

dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})


def create_app(conf, port):
    app = Flask(__name__)

    metrics = PrometheusMetrics(app)
    metrics.info('app_info', 'Github API Cache Server', version='1.0')

    config = CacheServerConfig(conf, port)
    cache = Cache(config)
    api_service = ApiService(config, cache)
    api_service.update_cache()

    @app.route('/', methods=['GET'])
    @metrics.counter('github_base_url', 'Number of invocations for github_base_url')
    def github_base_url():
        return jsonify(api_service.fetch_data(""))

    @app.route(NETFLIX_BASE_URL, methods=['GET'])
    @metrics.counter('netflix_base_url', 'Number of invocations for netflix_base_url')
    def netflix_base_url():
        return jsonify(api_service.netflix_base_url())

    @app.route(NETFLIX_MEMBERS_URL, methods=['GET'])
    @metrics.counter('netflix_members_url', 'Number of invocations for netflix_members_url')
    def netflix_members_url():
        return jsonify(api_service.netflix_members_url())

    @app.route(NETFLIX_REPOS_URL, methods=['GET'])
    @metrics.counter('netflix_repos_url', 'Number of invocations for netflix_repos_url')
    def netflix_repos_url():
        return jsonify(api_service.netflix_repos_url())

    @app.route(HEALTH_CHECK_URL)
    @metrics.counter('health_check_url', 'Number of invocations for health_check_url')
    def health_check():
        return Response(status=200) if api_service.health_check(HEALTH_CHECK_URL) is True else Response(status=503)

    @app.route(TOP_N_REPOS_BY_STARS, methods=['GET'])
    @metrics.counter('top_repos_by_stars_url', 'Number of invocations for top_repos_by_starsurl',
                     labels={'item_type': lambda: request.view_args['n']})
    def top_n_repos_by_stars(n):
        return jsonify(api_service.get_top_n_repos_by_stars(n))

    @app.route(TOP_N_REPOS_BY_FORKS, methods=['GET'])
    def top_n_repos_by_forks(n):
        return jsonify(api_service.get_top_n_repos_by_forks(n))

    @app.route(TOP_N_REPOS_BY_OPEN_ISSUES, methods=['GET'])
    def top_n_repos_by_open_issues(n):
        return jsonify(api_service.get_top_n_repos_by_open_issues(n))

    @app.route(TOP_N_REPOS_BY_LAST_UPDATED_TIME, methods=['GET'])
    def top_n_repos_by_last_updated_time(n):
        return jsonify(api_service.get_top_n_repos_by_last_updated(n))

    @app.route("/favicon.ico", methods=['GET'])
    def get_favicon():
        return Response(status=204)

    @app.route("/<path:proxy_path>")
    def proxy(proxy_path):
        return jsonify(api_service.proxy("/" + proxy_path))

    # Setup scheduler to update the cache
    scheduler = BackgroundScheduler()
    scheduler.add_job(func=api_service.update_cache, trigger="interval", seconds=config.refresh_interval_in_seconds)
    scheduler.start()

    # Shutdown your cron thread if the web process is stopped
    atexit.register(lambda: scheduler.shutdown())

    return app
