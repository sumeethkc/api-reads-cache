#!/usr/bin/env python
from pyhocon import ConfigFactory
import os


class CacheServerConfig:
    def __init__(self, file: str, port: int):
        """
        :param file: Config file location
        :param port: Port number for the web-server to run
        """
        config = ConfigFactory.parse_file(file)
        self.port = port
        self.refresh_interval_in_seconds = config.get_int("config.refreshIntervalInSeconds")
        self.baseUrl = config.get_string("config.baseUrl")
        self.github_token = os.environ.get('GITHUB_API_TOKEN')

        # Memcached config
        self.memcached_url = config.get_string("config.memcachedUrl")
        self.memcached_port = config.get_int("config.memcachedPort")
