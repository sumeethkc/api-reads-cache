#!/usr/bin/env python
from pymemcache.client import base
from project.config.cache_server_config import CacheServerConfig


class Cache:
    def __init__(self, config: CacheServerConfig):
        """
        :param config: CacheServerConfig object. Initializes memcached client
        """
        self.client = base.Client((config.memcached_url, config.memcached_port))

    def set(self, key: str, value: bytes) -> str:
        """
        :param key:
        :param value:
        :return: If no exception is raised, always returns True. If an exception is
          raised, the set may or may not have occurred. If noreply is True,
          then a successful return does not guarantee a successful set.
        """
        return self.client.set(key, value)

    def get(self, key: str) -> bytes:
        """
        :param key:
        :return: The value for the key, or default if the key wasn't found.
        """
        return self.client.get(key, None)

    def delete(self, key: str) -> bool:
        """
        :param key:
        :return: If ``noreply`` is True (or if it is unset and ``self.default_noreply``
          is True), the return value is always True. Otherwise returns True if
          the key was deleted, and False if it wasn't found.
        """
        return self.client.delete(key, False)
