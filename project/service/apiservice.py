#!/usr/bin/env python
import json
import logging
import time

import requests

from project.constants import *
from project.config.cache_server_config import CacheServerConfig
from project.persistence.cache import Cache


class ApiService:
    def __init__(self, config: CacheServerConfig, cache: Cache):
        self.config = config
        self.cache = cache
        self.logger = logging.getLogger(__name__)

    def fetch_data(self, endpoint: str) -> list:
        """
        :param endpoint: github endpoint to fetch the data from
        :return: response data either from cache or from web
        """
        try:
            cache_key = get_cache_key(endpoint)
            self.logger.info(cache_key)
            if self.cache.get(cache_key) is not None:
                self.logger.info("serving from cache")
                return json.loads(self.cache.get(cache_key).decode('utf-8'))
            else:
                data = self.proxy(endpoint)
                self.cache.set(cache_key, json.dumps(data).encode('utf-8'))
                return data
        except Exception as e:
            self.logger.error(e)

    def proxy(self, endpoint: str) -> list:
        """
        :param endpoint: github endpoint to fetch the data from
        :return: response data from web
        """
        self.logger.info("serving from web for %s" % endpoint)
        response = requests.get(self.config.baseUrl + endpoint,
                                headers={'Authorization': 'Bearer %s' % self.config.github_token})
        data = response.json()
        while 'next' in response.links.keys():
            response = requests.get(response.links['next']['url'],
                                    headers={'Authorization': 'Bearer %s' % self.config.github_token})
            data.extend(response.json())

        return data

    def netflix_repos_url(self) -> list:
        """
        :return: response data from /orgs/Netflix/repos
        """
        return self.fetch_data(NETFLIX_REPOS_URL)

    def netflix_members_url(self) -> list:
        """
        :return: response data from /orgs/Netflix/members
        """
        return self.fetch_data(NETFLIX_MEMBERS_URL)

    def netflix_base_url(self) -> list:
        """
        :return: response data from /orgs/Netflix
        """
        return self.fetch_data(NETFLIX_BASE_URL)

    def get_top_n_repos_by_stars(self, n: int) -> list:
        """
        :param n: integer to limit the list of entries to be returned
        :return: top n repos by stargazers_count
        """
        repos = self.fetch_data(NETFLIX_REPOS_URL)
        repos.sort(key=lambda x: x['stargazers_count'], reverse=True)
        return self.process_repos(repos, n, 'full_name', 'stargazers_count')

    def get_top_n_repos_by_forks(self, n: int) -> list:
        """
        :param n: integer to limit the list of entries to be returned
        :return: top n repos by forks_count
        """
        repos = self.fetch_data(NETFLIX_REPOS_URL)
        repos.sort(key=lambda x: x['forks_count'], reverse=True)
        return self.process_repos(repos, n, 'full_name', 'forks_count')

    def get_top_n_repos_by_open_issues(self, n: int) -> list:
        """
        :param n: integer to limit the list of entries to be returned
        :return: top n repos by open_issues_count
        """
        repos = self.fetch_data(NETFLIX_REPOS_URL)
        repos.sort(key=lambda x: x['open_issues_count'], reverse=True)
        return self.process_repos(repos, n, 'full_name', 'open_issues_count')

    def get_top_n_repos_by_last_updated(self, n: int) -> list:
        """
        :param n: integer to limit the list of entries to be returned
        :return: top n repose by updated_at time
        """
        repos = self.fetch_data(NETFLIX_REPOS_URL)
        repos.sort(key=lambda x: time.mktime(time.strptime(x['updated_at'], "%Y-%m-%dT%H:%M:%SZ")), reverse=True)
        return self.process_repos(repos, n, 'full_name', 'updated_at')

    def health_check(self, endpoint: str) -> bool:
        """
        :param endpoint: health check endpoint
        :return: bool to indicate health of service. True if cache is up and /orgs/Netflix is reachable. False otherwise
        """
        try:
            self.logger.info("Health Check")
            cache_key = get_cache_key(endpoint)
            if json.loads(self.cache.get(cache_key).decode('utf-8')) is not None and self.netflix_base_url() is not None:
                return True
            else:
                return False
        except Exception as e:
            self.logger.error(e)
            return False

    def update_cache(self) -> bool:
        """
        :return: refreshes cache by fetching data from all the supported end points
        """
        error = False
        data = self.proxy("")
        if data is None:
            error = True
        else:
            cache_key = get_cache_key("")
            try:
                self.cache.set(cache_key, json.dumps(data).encode('utf-8'))
            except Exception as e:
                self.logger.error(e)

            for endpoint in [NETFLIX_BASE_URL, NETFLIX_REPOS_URL, NETFLIX_MEMBERS_URL]:
                if error:
                    break
                else:
                    try:
                        data = self.proxy(endpoint)
                        cache_key = get_cache_key(endpoint)
                        self.cache.set(cache_key, json.dumps(data).encode('utf-8'))
                    except Exception as e:
                        self.logger.error(e)
                        error = True

        # Update cache health
        cache_key = get_cache_key(HEALTH_CHECK_URL)
        try:
            if not error:
                self.logger.info("Setting health ready key")
                self.cache.set(cache_key, json.dumps("ready").encode('utf-8'))
            else:
                self.logger.info("Deleting health ready key")
                self.cache.delete(cache_key)
        except Exception as e:
            self.logger.error(e)

        return error

    @staticmethod
    def process_repos(repos: list, n: int, full_name: str, criteria: str):
        """
        :param repos: list of repos returned from the endpoint
        :param n: integer to limit the number of entries
        :param full_name: Full Name of the repo
        :param criteria: sort criteria
        :return:
        """
        res = []
        for repo in repos:
            res.append((repo[full_name], repo[criteria]))

        if n >= len(res):
            return res
        else:
            return res[:n]
