#!/usr/bin/env python

GITHUB_BASE_URL_KEY = "githubBaseUrl"
NETFLIX_BASE_URL_KEY = "netflixBaseUrl"
NETFLIX_MEMBERS_KEY = "netflixMembers"
NETFLIX_REPOS_KEY = "netflixRepos"
HEALTH_CHECK_KEY = "isHealthy"

GITHUB_BASE_URL = "https://api.github.com"
NETFLIX_BASE_URL = "/orgs/Netflix"
NETFLIX_MEMBERS_URL = "/orgs/Netflix/members"
NETFLIX_REPOS_URL = "/orgs/Netflix/repos"
HEALTH_CHECK_URL = "/healthcheck"

TOP_N_REPOS_BY_STARS = "/view/top/<int:n>/stars"
TOP_N_REPOS_BY_FORKS = "/view/top/<int:n>/forks"
TOP_N_REPOS_BY_LAST_UPDATED_TIME = "/view/top/<int:n>/last_updated"
TOP_N_REPOS_BY_OPEN_ISSUES = "/view/top/<int:n>/open_issues"


def get_cache_key(endpoint):
    if endpoint == GITHUB_BASE_URL:
        return GITHUB_BASE_URL_KEY
    elif endpoint == NETFLIX_BASE_URL:
        return NETFLIX_BASE_URL_KEY
    elif endpoint == NETFLIX_MEMBERS_URL:
        return NETFLIX_MEMBERS_KEY
    elif endpoint == NETFLIX_REPOS_URL:
        return NETFLIX_REPOS_KEY
    elif endpoint == HEALTH_CHECK_URL:
        return HEALTH_CHECK_KEY
    else:
        return "default"
