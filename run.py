#!/usr/bin/env python
import argparse

from waitress import serve

from project import create_app

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Github API Cache')
    parser.add_argument('--port', dest='port', action='store',
                        type=int, default=5000,
                        help='port to run the server on')
    parser.add_argument('--config', dest='conf', action='store',
                        type=str, default="conf/custom/custom_application.conf")
    args = parser.parse_args()
    app = create_app(args.conf, args.port)
    serve(app, host="0.0.0.0", port=args.port)
