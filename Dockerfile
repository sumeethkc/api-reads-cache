FROM python:alpine

EXPOSE 5000

WORKDIR /project

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . .

# Commands in a list
CMD ["python", "run.py", "--port=5000", "--conf=conf/custom/custom_docker_application.conf"]